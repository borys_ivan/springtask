package spring.validate;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import spring.entity.Message;

import java.util.regex.Pattern;


@Service
public class MessageValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Message.class.equals(aClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        Message m = (Message) obj;
        Pattern PHONE_REGEX = Pattern.compile("^[0-9\\s\\-()]{14}$");
        Pattern EMAIL_REGEX = Pattern.compile("^[\\w\\d._-]+@[\\w\\d.-]+\\.[\\w\\d]{2,6}$");


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "phone.required");
        if (m.getPhone().length() < 14) {
            errors.rejectValue("phone", "phone.negative.phone");
        }

        if (m.getPhone() != null && !PHONE_REGEX.matcher(m.getPhone()).matches()) {
            errors.rejectValue("phone", "phone.negative.onlyNumber");
        }


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "email.required");

        if (m.getEmail().contains(" ") || !EMAIL_REGEX.matcher(m.getEmail()).matches()) {
            errors.rejectValue("email", "email.negative.correct");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "message", "message.required");

    }
}

