package spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import spring.entity.Message;
import spring.service.MessageService;


@Controller
//@RequestMapping("/main")
public class MessageController {


    @Autowired
    @Qualifier("messageValidator") // spring validator
    private Validator messageValidator;


    @Autowired
    private MessageService messageService;

    @GetMapping("/list")
    public String listPostcodes(Model theModel) {
        List<Message> theMessage = messageService.getMessages();
        theModel.addAttribute("message", theMessage);
        return "list-message";
    }

    //@GetMapping("/showForm")
    @GetMapping("/")
    public String showFormForAdd(Model theModel) {

        Message theMessage = new Message();
        theModel.addAttribute("massage", theMessage);
        return "add_message";
    }

    @PostMapping("/saveMassage")
    public String saveMessage(@Validated @ModelAttribute("message") Message theMessage, BindingResult bindingResult, Model model) {

        messageValidator.validate(theMessage, bindingResult);

        if (bindingResult.hasErrors()) {
            return "message_edit";
        }

        messageService.saveMessage(theMessage);

        model.addAttribute("phone",theMessage.getPhone());
        model.addAttribute("email",theMessage.getEmail());
        model.addAttribute("mess",theMessage.getMessage());
        return "added_success";
    }

}
