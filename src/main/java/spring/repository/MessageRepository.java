package spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import spring.entity.Message;

@Repository("messageRepository")
public interface MessageRepository extends JpaRepository<Message, Integer> {

}
