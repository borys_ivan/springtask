package spring.service;

import java.util.List;

import spring.entity.Message;

public interface MessageService {

    public List <Message> getMessages();

    public void saveMessage(Message theMessage);


}
