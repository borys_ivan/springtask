package spring.service;

import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import spring.repository.MessageRepository;
import spring.entity.Message;

@Service
public class PostcodeServiceImpl implements MessageService {

    @Autowired
    @Qualifier("messageRepository")
    private MessageRepository messageRepository;

    @Override
    @Transactional
    public List<Message> getMessages() {
        return messageRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    @Override
    public void saveMessage(Message theMessage) {
        messageRepository.save(theMessage);
    }

}

