<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="span" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Message</title>
</head>
<body>
<h3>
    Enter Message.
</h3>
<div class="col-md-offset-2 col-md-7">
<form:form method="POST" modelAttribute="message" action="saveMassage">
    <div class="form-group">
        Phone:
        <form:input path="phone" cssClass="form-control"/>
    </div>
    <div class="form-group">
    <span:errors path="phone" cssClass="alert alert-danger"/>
    </div>

    <div class="form-group">
        Email:
        <form:input path="email" cssClass="form-control"/>
    </div>
    <div class="form-group">
       <form:errors path="email" cssClass="alert alert-danger"/>
    </div>

    <div class="form-group">
        Message:
        <form:textarea path="message" cssClass="form-control"/>
    </div>
    <div class="form-group">
    <form:errors path="message" cssClass="alert alert-danger"/>
    </div>


    <button type="submit" class="btn btn-primary">Edit</button>
</form:form>

</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
        integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>

</body>
</html>